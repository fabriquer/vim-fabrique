"
" Vim syntax file for Fabrique.
"

if exists("b:current_syntax")
  finish
endif

let b:current_syntax = "fab"

" Literals
syn match	Boolean		'\<true\>'
syn match	Boolean		'\<false\>'
syn match	Number		'\<[0-9]\+\>'
syn match	Float		'\<[0-9\.]\+\>'

" Identifiers
syn match	Identifier	display '\<[_a-zA-Z][_a-zA-Z0-9]*\>' skipwhite
syn match	Constant	display '\<[A-Z][_a-zA-Z0-9]*\>' skipwhite

" Keywords
syn keyword	Conditional	if else
syn keyword	Debug		error print
syn keyword	Define		action
syn keyword	Function	function
syn keyword	Include		import
syn keyword	Macro		args buildroot srcroot subdir
syn keyword	Repeat		for
syn keyword	Structure	record
syn keyword	Tag		in out
syn keyword	Type		bool int string list file type
syn keyword	Typedef		type

" Operators
syn match	Operator	'+'
syn match	Operator	'-'
syn match	Operator	'*'
syn match	Operator	'/'
syn match	Operator	'?'
syn match	Operator	'\<::\>'
syn match	Operator	'\<:::\>'
syn match	Operator	'\<==\>'
syn match	Operator	'\<!=\>'
syn match	Operator	'\<and\>'
syn match	Operator	'\<not\>'
syn match	Operator	'\<or\>'
syn match	Operator	'\<xor\>'

" Other symbols
syn match	Special		'='
syn match	Special		':'
syn match	Special		','
syn match	Special		'\.'
syn match	Special		'\<<-\>'
syn match	Special		'[\[\]{}()]'
syn match	Special		'\<->\>'
syn match	Special		';'


"
" Multi-file syntax:
"
syn region	fabFiles	matchgroup=Delimiter start=/files(/ end=/[,)]/

syn match	Constant	"\<[A-Za-z0-9\-_\.]\+\>" contained containedin=fabFiles
syn match	Error		".*[(#\$].*" skipwhite contained containedin=fabFiles


"
" Strings:
"
syn region	String		start=+[bB]\='+ skip=+\\\\\|\\'\|\\$+ excludenl end=+'+ end=+$+ keepend
syn region	String		start=+[bB]\="+ skip=+\\\\\|\\"\|\\$+ excludenl end=+"+ end=+$+ keepend
syn region	String		start=+[bB]\="""+ end=+"""+ keepend
syn region	String		start=+[bB]\='''+ end=+'''+ keepend

syn match	Special		"${.*}" contained containedin=String


" Comments
syn match	Comment		"#.*$" display contains=Todo
syn keyword	Todo		TODO FIXME XXX contained
